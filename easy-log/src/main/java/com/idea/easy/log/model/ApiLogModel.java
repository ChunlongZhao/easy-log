package com.idea.easy.log.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @className: ApiLogModel
 * @description:
 * @author: salad
 * @date: 2022/6/1
 **/

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ApiLogModel extends LogCommonModel{
    protected static final long serialVersionUID = 1L;

    /**
     * 日志模块名
     */
    private String title;

}
