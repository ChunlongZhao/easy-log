package com.idea.easy.log.publisher;

/**
 * @className: ILogPublisher
 * @description: 事件发布者的接口
 * @author: salad
 * @date: 2022/6/3
 **/
public interface ILogPublisher {

    void publish();

}
