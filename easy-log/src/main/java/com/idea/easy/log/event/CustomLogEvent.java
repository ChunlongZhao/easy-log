package com.idea.easy.log.event;

import com.idea.easy.log.model.CustomLogModel;
import org.springframework.context.ApplicationEvent;

/**
 * @className: CustomLogEvent
 * @description: 自定义日志的事件
 * @author: salad
 * @date: 2022/6/9
 **/
public class CustomLogEvent extends ApplicationEvent {

	public CustomLogEvent(CustomLogModel source) {
		super(source);
	}

}
