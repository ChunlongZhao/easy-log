package com.idea.easy.log.annotation;

import java.lang.annotation.*;

/**
 * @className: ApiLog
 * @description: api日志注解
 * @author: salad
 * @date: 2022/6/1
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiLog {

    /**
     * 日志模块名称
     */
    String value();


}
